
<li>
    <div class="post-media"><a href="<?php the_permalink() ?>"><?php
                            if (has_post_thumbnail())
                            the_post_thumbnail('thumbnail');
                        ?></a></div>
    <h3 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title()?></a></h3>
    <div class="post-info"><a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><?php comments_number()?></a></div>
</li>

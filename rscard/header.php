<!DOCTYPE html>
<html lang="en" class=" theme-color-07cb79 theme-skin-light">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Favicon -->
	<link rel="shortcut icon" type="image/ico" href="<?php echo get_bloginfo('template_directory'); ?>/img/favicon.png"/>

	<!-- Google Fonts -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Fredoka+One">
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic">

	<!-- Icon Fonts -->
	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/fonts/map-icons/css/map-icons.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/fonts/icomoon/style.css">

    <!-- Styles -->                   <!--METTRE DANS CES FICHIERS LES LIENS A L'EMPLACEMENT DU FICHIER STYLE--->
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/js/plugins/jquery.bxslider/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/js/plugins/jquery.customscroll/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/js/plugins/jquery.mediaelement/mediaelementplayer.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/js/plugins/jquery.fancybox/jquery.fancybox.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/js/plugins/jquery.owlcarousel/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/js/plugins/jquery.owlcarousel/owl.theme.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/js/plugins/jquery.optionpanel/option-panel.css">
    <link href="<?php echo get_bloginfo('template_directory'); ?>/style.css" rel="stylesheet"> <!--LIEN A L'EMPLACEMENT DU FICHIER STYLE--->
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/colors/theme-color.css">

	<!-- Modernizer for detect what features the user’s browser has to offer -->
	<script type="text/javascript" src="<?php echo get_bloginfo('template_directory'); ?>/js/libs/modernizr.js"></script>

	<?php wp_head() ?>
</head>


<!---------------------------------------------------------------------PHONE--------MENU NAVIGATION----->
<div class="mobile-nav">
        <button class="btn-mobile mobile-nav-close"><i class="rsicon rsicon-close"></i></button>
		
        <div class="mobile-nav-inner">
            <nav id="mobile-nav" class="nav">
				<ul class="clearfix">
					<li><a href="<?php echo home_url(); ?>#about">About</a></li>
					<li><a href="index.html#skills">Skills</a></li>
					<li><a href="index.html#portfolio">Portfolio</a> </li>
					<li><a href="index.html#experience">Experience</a></li>
					<li><a href="index.html#references">References</a></li>
					<li> 
						<a href="blog.php">Blog</a>
						<ul>
							<li><a href="single-image.html">Image Post</a></li>
							<li><a href="single-slider.html">Slider Post</a></li>
							<li><a href="single-video.html">Video Post</a></li>
							<li><a href="single-audio.html">Audio Post</a></li>
							<li><a href="single-vimeo.html">Vimeo Post</a></li>
							<li><a href="single-youtube.html">Youtube Post</a></li>
							<li><a href="single-dailymotion.html">Dailymotion Post</a></li>
							<li><a href="single.html">Without Media Post</a></li>
							<li><a href="typography.html">Typography Page</a></li>
							<li><a href="icons.html">Icons Page</a></li>
							<li><a href="404.html">404 Page</a></li>
						</ul>
					</li>
					<li><a href="index.html#calendar">Calendar <span></span></a></li>
					<li><a href="index.html#contact">Contact <span></span></a></li>
				</ul>
			</nav>
        </div>
    </div><!-- .mobile-nav -->



<!-----------------------------APPEL AU SIDEBAR------------------------------>
<?php get_sidebar(); ?>
<!--------------------------------------------------------------------------->
<!------------------------------------------------------------------------ORDI--------MENU NAVIGATION----->
<div class="wrapper">
        <header class="header">
            <div class="head-bg" style="background-image: url('<?php echo get_bloginfo('template_directory'); ?>/img/uploads/rs-cover.jpg')"></div>

            <div class="head-bar">
                <div class="head-bar-inner">
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">                            
						<a class="logo" href="<?php echo home_url() ?>"><span>RP</span>crea</a>							<!-- <a class="head-logo" href=""><img src="<?php echo get_bloginfo('template_directory'); ?>/img/rs-logo.png" alt="RScard"/></a> -->
                        </div>

                        <div class="col-sm-9 col-xs-6">
                            <div class="nav-wrap">
                                <nav id="nav" class="nav">
									<?php
                                    wp_nav_menu(array(
                                        'theme_location' => 'primary',
                                        'container' => false,
                                        'menu_class' => 'clearfix',
                                    ));
                                    ?>
								</nav>

                                <button class="btn-mobile btn-mobile-nav">Menu</button>
                                <button class="btn-sidebar btn-sidebar-open"><i class="rsicon rsicon-menu"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header><!-- .header -->
<!-----------------------------APPEL AU HEADER (+NAV)------------------------>
<?php get_header(); ?>
<!--------------------------------------------------------------------------->
<!-----------------------------------CORPS----------------------------------->
<div class="content">
    <div class="container">
        <!-- START: PAGE CONTENT -->
        <div class="blog">
            <div class="blog-grid">
                <div class="grid-sizer"></div>

                <!--ARTICLE IMAGE SINGLE---->

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php get_template_part('article'); ?>
                    <?php endwhile;
            endif; ?>


            </div><!-- .pagination -->
        </div><!-- .blog -->
        <!-- END: PAGE CONTENT -->

    </div><!-- .container -->
</div><!-- .content -->


        <?php get_footer() ?>
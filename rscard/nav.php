<div class="mobile-nav">
        <button class="btn-mobile mobile-nav-close"><i class="rsicon rsicon-close"></i></button>
		
        <div class="mobile-nav-inner">
            <nav id="mobile-nav" class="nav">
				<ul class="clearfix">
					<li><a href="<?php echo home_url(); ?>#about">About</a></li>
					<li><a href="index.html#skills">Skills</a></li>
					<li><a href="index.html#portfolio">Portfolio</a> </li>
					<li><a href="index.html#experience">Experience</a></li>
					<li><a href="index.html#references">References</a></li>
					<li> 
						<a href="blog.php">Blog</a>
						<ul>
							<li><a href="single-image.html">Image Post</a></li>
							<li><a href="single-slider.html">Slider Post</a></li>
							<li><a href="single-video.html">Video Post</a></li>
							<li><a href="single-audio.html">Audio Post</a></li>
							<li><a href="single-vimeo.html">Vimeo Post</a></li>
							<li><a href="single-youtube.html">Youtube Post</a></li>
							<li><a href="single-dailymotion.html">Dailymotion Post</a></li>
							<li><a href="single.html">Without Media Post</a></li>
							<li><a href="typography.html">Typography Page</a></li>
							<li><a href="icons.html">Icons Page</a></li>
							<li><a href="404.html">404 Page</a></li>
						</ul>
					</li>
					<li><a href="index.html#calendar">Calendar <span></span></a></li>
					<li><a href="index.html#contact">Contact <span></span></a></li>
				</ul>
			</nav>
        </div>
    </div><!-- .mobile-nav -->

<!------------------------------------------------------------------------ORDI--------MENU NAVIGATION----->
<div class="wrapper">
        <header class="header">
            <div class="head-bg" style="background-image: url('<?php echo get_bloginfo('template_directory'); ?>/img/uploads/rs-cover.jpg')"></div>

            <div class="head-bar">
                <div class="head-bar-inner">
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">                            
                            <a class="logo" href="index.html"><span>RP</span>crea</a>
							<!-- <a class="head-logo" href=""><img src="<?php echo get_bloginfo('template_directory'); ?>/img/rs-logo.png" alt="RScard"/></a> -->
                        </div>

                        <div class="col-sm-9 col-xs-6">
                            <div class="nav-wrap">
                                <nav id="nav" class="nav">
									<ul class="clearfix">
										<li><a href="index.html#about">About</a></li>
										<li><a href="index.html#skills">Skills</a></li>
										<li><a href="index.html#portfolio">Portfolio</a> </li>
										<li><a href="index.html#experience">Experience</a></li>
										<li><a href="index.html#references">References</a></li>
										<li>
											<a href="blog.php">Blog</a>
											
										</li>
										<li><a href="index.html#calendar">Calendar <span></span></a></li>
										<li><a href="index.html#contact">Contact <span></span></a></li>
									</ul>
								</nav>

                                <button class="btn-mobile btn-mobile-nav">Menu</button>
                                <button class="btn-sidebar btn-sidebar-open"><i class="rsicon rsicon-menu"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header><!-- .header -->
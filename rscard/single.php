
<!-----------------------------APPEL AU HEADER (+NAV)------------------------>
<?php get_header(); ?>
<!--------------------------------------------------------------------------->
<?php if (have_posts()): while (have_posts()): the_post(); ?> <!---la boucle pour que les element the_title etc soit pris en compte->
<!---------------------------------------------------------------------------PARTIE PRINCIPALE DE L'ARTICLE--------------->              
<div class="content">
    <div class="container">

			<!-- START: PAGE CONTENT -->			
			<div class="row animate-up">
				<div class="col-sm-8">

					<main class="post-single">
						<article class="post-content section-box">							
							<div class="post-inner">
								<header class="post-header">
									<div class="post-data">
										<div class="post-tag">
											<a href="single.html"><?php the_category('#'); ?></a>
										</div>

										<div class="post-title-wrap">
											<h1 class="post-title"><?php the_title(); ?></h1>
											<time class="post-datetime" datetime="2015-03-13T07:44:01+00:00">
                                                <span class="day"><?php echo date("j");?> </span>
                                                <span class="month"><?php echo date("M");?> </span>
                                            </time>
										</div>

										<div class="post-info">
											<a href="single.html"><i class="rsicon rsicon-user"></i><?php the_author(); ?></a>
											<a href="single.html"><i class="rsicon rsicon-comments"></i><?php comments_number(); ?></a>
										</div>
									</div>
								</header>
<!-----------------------------------------------------------------------------CORPS DE L'ARTICLE------------>
                                <div class="post-editor clearfix">    
                                    <?php the_content (); ?>
                                    <blockquote> 
                                        <cite><a href="single.html"><?php the_author(); ?></a></cite>
                                    </blockquote>
                                    <figure id="attachment_905" style="width: 509px;" class="wp-caption alignright">
                                        <?php
                                            if (has_post_thumbnail())
                                            the_post_thumbnail('');
                                        ?>
                                    <figcaption class="wp-caption-text"></figcaption>
                                    </figure>
                                </div>

<!------------------PETIT FOOTER DE L'ARTICLE--------------->  
                                <footer class="post-footer">
                                    <div class="post-share">
                                        <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-503b5cbf65c3f4d8" async="async"></script>
                                        <div class="addthis_sharing_toolbox"></div>
                                    </div>
                                </footer>
                            </div><!-- .post-inner -->
                        </article><!-- .post-content -->

<!-------------------------------------------------------------------------------------PAGINATION---------------> 
                        <nav class="post-pagination section-box">
                            <div class="post-next">
                                <div class="post-tag"><?php next_posts_link ('Older') ?><a href="single.html"><?php the_category('#'); ?></a></div>
                                <h3 class="post-title"><a href="single.html"><?php the_title(); ?></a></h3>

                                <div class="post-info">
                                    <a href="single.html"><i class="rsicon rsicon-user"></i><?php the_author() ?></a>
                                    <a href="single.html"><i class="rsicon rsicon-comments"></i><?php comments_number() ?></a>
                                </div>
                            </div>
                            <div class="post-prev">
                                <div class="post-tag"><?php previous_posts_link ('Newer') ?><a href="single.html"><?php the_category('#'); ?></a></div>
                                <h3 class="post-title"><a href="single.html"><?php the_title(); ?></a></h3>

                                <div class="post-info">
                                    <a href="single.html"><i class="rsicon rsicon-user"></i><?php the_author() ?></a>
                                    <a href="single.html"><i class="rsicon rsicon-comments"></i><?php comments_number() ?></a>
                                </div>
                            </div>
                        </nav><!-- .post-pagination -->

<!---------------------PAS BESOIN DE GERER-----------------------------------PARTIE DES COMMENTAIRES DE L'ARTICLE---------------> 
                        <div class="post-comments">
                            <h2 class="section-title"><?php comments_number() ?></h2>
                            
                            <div class="section-box">
                                <ol class="comment-list">
                                    <li class="comment">
                                        <article class="comment-body">
                                            <div class="comment-avatar">
                                                <img src="<?php echo get_bloginfo('template_directory'); ?>/<?php echo get_bloginfo('template_directory'); ?>/img/rs-avatar-64x64.jpg" alt="avatar"/>
                                            </div>
                                            <div class="comment-content">
                                                <div class="comment-meta">
                                                    <span class="name"><?php the_author() ?></span>
                                                    <time class="date" datetime="2015-03-20T13:00:14+00:00"><?php echo get_the_date() ?></time>
                                                    <a class="reply-link" href="single.html#comment-reply">Reply</a>
                                                </div>
                                                <div class="comment-message">
                                                    <p><?php //the_content (); ?></p>
                                                </div>
                                            </div>
                                        </article>

                                    </li><!-- .comment -->

                                </ol><!-- .comment-list -->

 <!---------------------------------------------------------------------------PARTIE POUR REDIGER UN COMMENTAIRE--------------->                 
                                <div id="comment-reply" class="comment-reply">
                                    <form>
                                        <div class="input-field">
                                            <input type="text" name="rs-comment-name"/>
                                            <span class="line"></span>
                                            <label>Name *</label>
                                        </div>

                                        <div class="input-field">
                                            <input type="email" name="rs-comment-email"/>
                                            <span class="line"></span>
                                            <label>Email *</label>
                                        </div>

                                        <div class="input-field">
                                            <input type="text" name="rs-comment-website"/>
                                            <span class="line"></span>
                                            <label>Website</label>
                                        </div>

                                        <div class="input-field">
                                            <textarea rows="4" name="rs-comment-message"></textarea>
                                            <span class="line"></span>
                                            <label>Type Comment Here *</label>
                                        </div>

                                        <div class="text-right">
                                            <span class="btn-outer btn-primary-outer ripple">
                                                <input class="btn btn-lg btn-primary" type="button" value="Leave Comment">
                                            </span>
                                        </div>
                                    </form>
                                </div><!-- .comment-reply -->
                            </div><!-- .section-box -->
                        </div><!-- .post-comments -->
                    </main>
                    <!-- .post-single -->
                </div>
                <div class="col-sm-4">
					<div class="sidebar sidebar-default">
						<div class="widget-area">
							<aside class="widget widget-profile">
								<div class="profile-photo">
                                    <?php
                                        if (has_post_thumbnail())
                                        the_post_thumbnail('thumbnail');
                                    ?>
								</div>
								<div class="profile-info">
									<h2 class="profile-title">Robert Smith</h2>
									<h3 class="profile-position">Developer and businessman</h3>
								</div>
							</aside><!-- .widget-profile -->

							<aside class="widget widget_search">
								<h2 class="widget-title">Search</h2>
								<form class="search-form">
									<label class="ripple">
										<span class="screen-reader-text">Search for:</span>
										<input class="search-field" type="search" placeholder="Search">
									</label>
									<input type="submit" class="search-submit" value="Search">
								</form>
							</aside><!-- .widget_search -->

							<aside class="widget widget_contact">
								<h2 class="widget-title">Contact Me</h2>
								<form class="contactForm" action="https://rscard.px-lab.com/html/php/contact_form.php" method="post">
									<div class="input-field">
										<input class="contact-name" type="text" name="name"/>
										<span class="line"></span>
										<label>Name</label>
									</div>

									<div class="input-field">
										<input class="contact-email" type="email" name="email"/>
										<span class="line"></span>
										<label>Email</label>
									</div>

									<div class="input-field">
										<input class="contact-subject" type="text" name="subject"/>
										<span class="line"></span>
										<label>Subject</label>
									</div>

									<div class="input-field">
										<textarea class="contact-message" rows="4" name="message"></textarea>
										<span class="line"></span>
										<label>Message</label>
									</div>

									<span class="btn-outer btn-primary-outer ripple">
										<input class="contact-submit btn btn-lg btn-primary" type="submit" value="Send"/>
									</span>
									
									<div class="contact-response"></div>
								</form>
							</aside><!-- .widget_contact -->

							<aside class="widget widget-popuplar-posts">
								<h2 class="widget-title">Popular posts</h2>
								<ul>
									<li>
                                        <?php get_template_part ('article-small'); ?>
                                    
                                   
                                        <?php get_template_part ('article-small'); ?>
                                    
                                    
                                        <?php get_template_part ('article-small'); ?>
									</li>
								</ul>
							</aside><!-- .widget-popuplar-posts -->

							<aside class="widget widget_tag_cloud">
								<h2 class="widget-title">Tag Cloud</h2>
								<div class="tagcloud">
									<a href="single.html" title="1 topic"><?php the_category(' '); ?></a>
								</div>
							</aside><!-- .widget_tag_cloud -->

							<aside class="widget widget-recent-posts">
								<h2 class="widget-title">Recent posts</h2>
								<ul>
									<li>
										<div class="post-tag">
											<a href="single.html">#Photo</a>
											<a href="single.html">#Architect</a>
										</div>
										<h3 class="post-title"><a href="single.html">Standard Post Format With Featured Image</a></h3>
										<div class="post-info"><a href="single.html"><i class="rsicon rsicon-comments"></i>56 comments</a></div>
									</li>
									<li>
										<div class="post-tag">
											<a href="single.html">#Photo</a>
											<a href="single.html">#Architect</a>
										</div>
										<h3 class="post-title"><a href="single.html">Standard Post Format With Featured Image</a></h3>
										<div class="post-info"><a href="single.html"><i class="rsicon rsicon-comments"></i>56 comments</a></div>
									</li>
									<li>
										<div class="post-tag">
											<a href="single.html">#Photo</a>
											<a href="single.html">#Architect</a>
										</div>
										<h3 class="post-title"><a href="single.html">Standard Post Format With Featured Image</a></h3>
										<div class="post-info"><a href="single.html"><i class="rsicon rsicon-comments"></i>56 comments</a></div>
									</li>
								</ul>
							</aside><!-- .widget-recent-posts -->

							<aside class="widget widget_categories">
								<h2 class="widget-title">Categories</h2>
								<ul>
									<li><a href="single.html" title="Architecture Category Posts">Architecture</a> (9)</li>
									<li><a href="single.html" title="Business Category Posts">Business</a> (16)</li>
									<li><a href="single.html" title="Creative Category Posts">Creative</a> (18)</li>
									<li><a href="single.html" title="Design Category Posts">Design</a> (10)</li>
									<li><a href="single.html" title="Development Category Posts">Development</a> (14)</li>
									<li><a href="single.html" title="Education Category Posts">Education</a> (9)</li>
								</ul>
							</aside><!-- .widget_categories -->
						</div><!-- .widget-area -->
					</div><!-- .sidebar -->
				</div><!-- .col-sm-4 -->
            </div><!-- .row -->
            
    </div><!-- .container -->
</div><!-- .content -->

<?php endwhile; endif; ?>

<!-----------------------------APPEL AU FOOTER------------------------------->
<?php get_footer(); ?>
<!---------------------------------------------------------------------------> 

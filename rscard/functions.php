
<?php
add_action('after_setup_theme', 'custom_theme_setup');
function custom_theme_setup()
{
    add_theme_support('post-thumbnails');

    //WordPress Titles
    add_theme_support('title-tag');


    register_nav_menus(array(
        'primary' => 'Menu principal',
    ));
}

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

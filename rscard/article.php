<!---presentation des articles en mode vignette sous onglet blog-->
<div class="grid-item">
    <article class="post-box animate-up">

            <div class="post-media">
                <div class="post-image">
                    <a href="<?php the_permalink() ?>">
                        <?php
                            if (has_post_thumbnail())
                            the_post_thumbnail('thumbnail');
                        ?>
                    </a>
                </div>
            </div>

            <div class="post-data">
            <time class="post-datetime" datetime="2015-03-13T07:44:01+00:00">
                <span class="day"><?php echo date("j");?></span>
				<span class="month"><?php echo date("M");?></span>
			</time>
                

                <div class="post-tag">
                    <a href="category.html"><?php the_category('#'); ?></a>
                </div>
                <h3 class="post-title">
                    <a href ="<?php the_permalink() ?>">
                        <?php the_title()?>
                    </a>
                </h3>
                <div class="post-info">
                    <a href="category.html"><i class="rsicon rsicon-user"></i><a href="#"><?php the_author() ?></a></a>
                    <a href="category.html"><i class="rsicon rsicon-comments"></i><?php comments_number() ?></a>
                </div>
            </div>

    </article>
</div>
